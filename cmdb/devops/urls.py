# _*_ coding: utf-8 _*_
__author__ = 'HaoChenXiao'
from django.conf.urls import url, include
from devops import views

urlpatterns = [
	url(r'^monitor_config_stop/$', views.MonitorConfigStopView.as_view(), name = 'monitor_config_stop'),
	url(r'^monitor_config_start/$', views.MonitorConfigStartView.as_view(), name = 'monitor_config_start'),
	url(r'^monitorconfig_edit/$', views.MonitorConfigEditView.as_view(), name = 'monitorconfig_edit'),
	url(r'^monitor_config_delete/$', views.MonitorConfigDeleteView.as_view(), name = 'monitor_config_delete'),
	url(r'^monitor_config/$', views.MonitorConfigView.as_view(), name = 'monitor_config'),
	url(r'^add_monitor/$', views.AddMonitorConfigView.as_view(), name='monitor_config'),
	url(r'^api_monitor/$', views.APIMonitorConfigView.as_view(), name='api_monitor'),
	url(r'^autorecovery/$', views.AutoRecoveryView.as_view(), name = 'auto_recovery'),
	url(r'^prometheus/$', views.PrometheusView.as_view(), name='auto_prometheus'),
	url(r'^api_autorecovery/$', views.APIAutoRecoveryView.as_view(), name='api_auto_recovery'),
	url(r'^api_prometheus/$', views.APIPrometheusView.as_view(), name='api_prometheus'),
	url(r'^autorecovery_start/$', views.AutoRecoveryStartView.as_view(), name = 'auto_recovery_start'),
	url(r'^autorecovery_stop/$', views.AutoRecoveryStopView.as_view(), name = 'auto_recovery_stop'),
	url(r'^prometheus_start/$', views.PrometheusStartView.as_view(), name='auto_prometheus_start'),
	url(r'^prometheus_stop/$', views.PrometheusStopView.as_view(), name='auto_prometheus_stop'),
	url(r'^autorecovery_edit/$', views.AutoRecoveryEditView.as_view(), name = 'autorecovery_edit'),
	url(r'^prometheus_edit/$', views.PrometheusEditView.as_view(), name='prometheus_edit'),
	url(r'^auto_recovery_delete/$', views.AutoRecoveryDeleteView.as_view(), name = 'auto_recovery_delete'),
	url(r'^auto_prometheus_delete/$', views.PrometheusDeleteView.as_view(), name='auto_prometheus_delete'),
	url(r'^chart_list/$', views.ChartListView.as_view(), name = 'chart_list'),
	url(r'^api_chart/$', views.APIChartView.as_view(), name='api_chart'),
	url(r'^chart_detail/$', views.ChartDetailView.as_view(), name = 'chart_detail'),
	url(r'^chart_add/$', views.ChartCreateView.as_view(), name = 'chart_create'),
	url(r'^chart_start/$', views.ChartStartView.as_view(), name = 'chart_start'),
	url(r'^chart_stop/$', views.ChartStopView.as_view(), name = 'chart_stop'),
	url(r'^ssh_console/$', views.ConsoleView.as_view(), name = 'ssh_web'),
	url(r'^prometheus_webhook/$', views.PrometheusWebhookViews, name='prometheus_webhook'),
]