# ~*~ coding: utf-8 ~*~
import json
import os
import importlib,sys
importlib.reload(sys)
import django
from devops.runner import AdHocRunner
from devops.callback import CommandResultCallback


def ansible_shell(assets,args):
    task_tuple = (('shell', args),)  ##例子，调用普通的模块命令
    hoc = AdHocRunner(hosts=assets)
    hoc.results_callback = CommandResultCallback()
    ret = hoc.run(task_tuple)
    #print(ret)
def ansible_setup(assets):
    task_tuple = (('setup',''),)  ##例子，调用setup，获取资产信息
    runner = AdHocRunner(assets)
    result = runner.run(task_tuple=task_tuple,pattern='all', task_name='Ansible Ad-hoc')
    #print (result)
    return result

def ansible_authorized_key(assets,args):
    task_tuple = (('authorized_key', args),)  ##例子，调用普通的模块命令
    hoc = AdHocRunner(hosts=assets)
    hoc.results_callback = CommandResultCallback()
    ret = hoc.run(task_tuple)
    return ret

