# Generated by Django 2.0.6 on 2021-12-15 11:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devops', '0039_auto_20211214_0936'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='autorecovery',
            name='alertime',
        ),
        migrations.RemoveField(
            model_name='autorecovery',
            name='fixtime',
        ),
    ]
