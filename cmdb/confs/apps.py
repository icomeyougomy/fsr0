from django.apps import AppConfig


class ConfsConfig(AppConfig):
    name = 'confs'
